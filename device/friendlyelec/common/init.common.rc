#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

on early-init
    mount debugfs debugfs /sys/kernel/debug
    chmod 0755 /sys/kernel/debug

on init
    # Support legacy paths
    symlink /sdcard /mnt/sdcard
    symlink /sdcard /storage/sdcard0

on late-init
    # boot time fs tune
    write /sys/block/mmcblk0/queue/iostats 0
    write /sys/block/mmcblk0/queue/read_ahead_kb 32
    write /sys/block/mmcblk0/queue/nr_requests 256

on fs
    mount_all fstab.${ro.hardware} --early
    swapon_all fstab.${ro.hardware}

on late-fs
    # Start bootanim and its dependencies
    start servicemanager
    start surfaceflinger
    start bootanim

    # mount RW partitions which need run fsck
    mount_all fstab.${ro.hardware} --late

on post-fs-data
    # Create the directories used by the Wireless subsystem
    mkdir /data/misc/wifi 0770 wifi wifi
    mkdir /data/misc/wifi/sockets 0770 wifi wifi
    mkdir /data/misc/wifi/wpa_supplicant 0770 wifi wifi
    mkdir /data/misc/dhcp 0770 dhcp dhcp
    chown dhcp dhcp /data/misc/dhcp

    # Create the directories used by rild
    mkdir /data/misc/rild 0770 system radio

    # Create /data/time folder for time-services
    mkdir /data/time/ 0700 system system

on boot
    # execute script to set initial CPU settings
    exec - root root system -- /system/bin/init.power.sh

    # Bluetooth
    chown bluetooth net_bt /sys/class/rfkill/rfkill0/type
    chown bluetooth net_bt /sys/class/rfkill/rfkill0/state
    chmod 0660 /sys/class/rfkill/rfkill0/state

    # Adjust socket buffer to enlarge TCP receive window for high bandwidth
    write /proc/sys/net/ipv4/tcp_adv_win_scale 1

    # Assign TCP buffer thresholds to be ceiling value of technology maximums
    # Increased technology maximums should be reflected here.
    write /proc/sys/net/core/rmem_max  8388608
    write /proc/sys/net/core/wmem_max  8388608

on property:sys.boot_completed=1
    # end boot time fs tune
    write /sys/block/mmcblk0/queue/nr_requests 128
    write /sys/block/mmcblk0/queue/read_ahead_kb 128
    write /sys/block/mmcblk0/queue/iostats 1

service wpa_supplicant /system/bin/wpa_supplicant \
    -iwlan0 -Dnl80211 -c/data/misc/wifi/wpa_supplicant.conf \
    -I/system/etc/wifi/wpa_supplicant_overlay.conf \
    -O/data/misc/wifi/sockets \
    -e/data/misc/wifi/entropy.bin -g@android:wpa_wlan0
    #   we will start as root and wpa_supplicant will switch to user wifi
    #   after setting up the capabilities required for WEXT
    #   user wifi
    #   group wifi inet keystore
    class main
    socket wpa_wlan0 dgram 660 wifi wifi
    disabled
    oneshot

service p2p_supplicant /system/bin/wpa_supplicant \
    -iwlan0 -Dnl80211 -c/data/misc/wifi/wpa_supplicant.conf \
    -I/system/etc/wifi/p2p_supplicant_overlay.conf \
    -O/data/misc/wifi/sockets -puse_p2p_group_interface=1 \
    -m/data/misc/wifi/p2p_supplicant.conf \
    -e/data/misc/wifi/entropy.bin -g@android:wpa_wlan0
    class main
    socket wpa_wlan0 dgram 660 wifi wifi
    disabled
    oneshot

